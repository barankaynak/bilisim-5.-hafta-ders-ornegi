﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication15.Controllers
{
    public class OgrenciController : Controller
    {
        // GET: Ogrenci
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bilgi()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BilgiKaydet([Bind(Include = "Ad,Numara")] Data.Ogrenci ogrenci)
        {
            if (string.IsNullOrEmpty(ogrenci.Ad))
            {
                ModelState.AddModelError("", "Öğrenci adı boş olamaz!");
                return View("Bilgi");
            }
            else
            {
                //DBye yaz
                Data.BilisimOgrenciTestEntities db = new Data.BilisimOgrenciTestEntities();
                db.Ogrenci.Add(ogrenci);
                db.SaveChanges();
            }

            return View(ogrenci);
        }
    }
}